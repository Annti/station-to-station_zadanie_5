# -*- coding: utf-8 -*-

"""
Aneta Ochedowska
"""

import socket
import getopt
import sys
from Utils import ruwnasie, variable_random, del_ruwnasie, \
    mod_pow, sanitize, sign, verify, encrypt, decrypt


class Client(object):
    def __init__(self, prime_number, generator,
                 server_public_key, client_private_key):
        self.prime_number = prime_number
        self.generator = generator
        self.server_public_key = server_public_key
        self.client_private_key = client_private_key
        self.server = None

    def connect_to_server(self):
        self.server = socket.socket()
        self.server.connect((SERVER_HOST, SERVER_PORT))

    def run(self):
        alpha = variable_random(self.prime_number)
        self.connect_to_server()
        message = mod_pow(self.generator, alpha, self.prime_number)
        self.server.send(ruwnasie(str(message)))
        server_request1 = del_ruwnasie(self.server.recv(512))
        server_request2 = del_ruwnasie(self.server.recv(512))

        if sanitize(server_request1, server_request2):
            self.server.send(ruwnasie('ERROR'))
            raise Exception()

        secret_key = mod_pow(server_request1, alpha, self.prime_number)

        signature = decrypt(server_request2, secret_key)

        s_and_c_val = '%d;%d' % (int(message), int(server_request1))

        verified = verify(self.server_public_key, s_and_c_val, signature)

        if not verified:
            self.server.send(ruwnasie('ERROR'))
            raise Exception()

        singnature_val = sign(self.client_private_key, s_and_c_val)
        self.server.send(ruwnasie(str(encrypt(singnature_val, secret_key))))

        print 'OK'


if __name__ == '__main__':

    SERVER_HOST = None
    SERVER_PORT = None
    PRIME_NUMBER = None
    GENERATOR = None
    SERVER_PUBLIC_KEY = None
    CLIENT_PRIVATE_KEY = None

    try:
        OPTS, ARGS = getopt.getopt(sys.argv[1:], "o:p:P:G:K:S:")

        for opt, arg in OPTS:
            if opt == '-o':
                SERVER_HOST = arg
            if opt == '-p':
                SERVER_PORT = int(arg)
            if opt == '-G':
                GENERATOR = arg
            if opt == '-P':
                PRIME_NUMBER = arg
            if opt == '-K':
                SERVER_PUBLIC_KEY = arg
            if opt == '-S':
                CLIENT_PRIVATE_KEY = arg

        CLIENT = Client(prime_number=PRIME_NUMBER,
                        generator=GENERATOR,
                        server_public_key=SERVER_PUBLIC_KEY,
                        client_private_key=CLIENT_PRIVATE_KEY)

        CLIENT.run()

    except Exception:
        print 'ERROR'
        sys.exit(2)
