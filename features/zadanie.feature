Feature: Zdanie z BSK

  Scenario: Proste polaczenie z klientem

    Given Wlaczony serwer z nastepujacymi ustawieniami
      | SERVER_PORT | PRIME_NUMBER | GENERATOR | CLIENT_PUBLIC_KEY  | SERVER_PRIVATE_KEY  |
      | 456         | 5            | 11        | pem/client_pub.pem | pem/server_priv.pem |


    When Klient laczy sie z serwerem uzywajac danych
      | SERVER_HOST | SERVER_PORT | PRIME_NUMBER | GENERATOR | SERVER_PUBLIC_KEY  | CLIENT_PRIVATE_KEY  |
      | localhost   | 456         | 5            | 11        | pem/server_pub.pem | pem/client_priv.pem |

    Then Wypisz logi na ekran
