from ProcessHandler import ProcessHandler
from behave import *

use_step_matcher("re")


@given("Wlaczony serwer z nastepujacymi ustawieniami")
def step_impl(context):
    conf = context.table[0]

    command = [
        "Server.py",
        "-p", conf['SERVER_PORT'],
        "-P", conf['PRIME_NUMBER'],
        "-G", conf['GENERATOR'],
        "-K", conf['CLIENT_PUBLIC_KEY'],
        "-S", conf['SERVER_PRIVATE_KEY'],
    ]


    context.server = ProcessHandler(command,
                                    log_file="logs/server_log.txt",
                                    name="Server")


@when("Klient laczy sie z serwerem uzywajac danych")
def step_impl(context):
    conf = context.table[0]

    command = [
        "Client.py",
        "-o", conf['SERVER_HOST'],
        "-p", conf['SERVER_PORT'],
        "-P", conf['PRIME_NUMBER'],
        "-G", conf['GENERATOR'],
        "-K", conf['SERVER_PUBLIC_KEY'],
        "-S", conf['CLIENT_PRIVATE_KEY'],
    ]

    context.client = ProcessHandler(command,
                                    log_file="logs/client_log.txt",
                                    name="Client")
    context.client.join()


@then("Wypisz logi na ekran")
def step_impl(context):
    context.server.log()
    context.client.log()
