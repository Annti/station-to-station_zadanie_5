import os

import subprocess

import sys


class ProcessHandler():
    def __init__(self, command, log_file, name):
        self.name = name
        self.log_file = log_file

        dir = os.path.dirname(log_file)
        if not os.path.exists(dir):
            os.makedirs(dir)

        with open(log_file, 'w') as log:
            self.server = subprocess.Popen(
                [sys.executable] + command,
                stdout=log,
                stderr=log
            )

    def log(self):
        print("Logi procesu '{}':".format(self.name))
        with open(self.log_file, 'r') as log:
            print(log.read())

    def join(self):
        self.server.wait()

    def __del__(self):
        self.server.kill()
