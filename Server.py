# -*- coding: utf-8 -*-
"""
Aneta Ochedowska
"""

import sys
import socket
import getopt
from Utils import ruwnasie, variable_random, del_ruwnasie, \
    mod_pow, sanitize, sign, verify, encrypt, decrypt


class Server(object):
    """ Main Server class """

    def __init__(self, prime_number, generator,
                 client_public_key, server_private_key):
        self.prime_number = prime_number
        self.generator = generator
        self.client_public_key = client_public_key
        self.server_private_key = server_private_key
        self.server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    def bind(self):
        """ bind method """
        self.server.bind((SERVER_HOST, SERVER_PORT))
        self.server.listen(5)

    def run(self):
        """ run method """
        client = self.server.accept()[0]

        client_request_1 = del_ruwnasie(str(client.recv(512)))

        if sanitize(client_request_1):
            client.send(ruwnasie('ERROR'))
            client.send(ruwnasie('ERROR'))
            raise Exception()

        betha = variable_random(self.prime_number)

        message = mod_pow(self.generator, betha, self.prime_number)

        secret_key = mod_pow(client_request_1, betha, self.prime_number)

        client.send(ruwnasie(str(message)))

        s_and_c_val = '%d;%d' % (int(client_request_1), int(message))

        singnature_val = sign(self.server_private_key, s_and_c_val)

        client.send(ruwnasie(str(encrypt(singnature_val, secret_key))))

        client_request_2 = del_ruwnasie(str(client.recv(512)))

        if sanitize(client_request_2):
            raise Exception()

        signature = decrypt(client_request_2, secret_key)

        verified = verify(self.client_public_key, s_and_c_val, signature)

        if not verified:
            raise Exception()

        client.close()


if __name__ == '__main__':
    SERVER_HOST = 'localhost'
    SERVER_PORT = None
    PRIME_NUMBER = None
    GENERATOR = None
    CLIENT_PUBLIC_KEY = None
    SERVER_PRIVATE_KEY = None

    try:
        OPTS, ARGS = getopt.getopt(sys.argv[1:], "p:P:G:K:S:")

        for opt, arg in OPTS:
            if opt == '-p':
                SERVER_PORT = int(arg)
            if opt == '-P':
                PRIME_NUMBER = arg
            if opt == '-G':
                GENERATOR = arg
            if opt == '-K':
                CLIENT_PUBLIC_KEY = arg
            if opt == '-S':
                SERVER_PRIVATE_KEY = arg

        SERVER = Server(prime_number=PRIME_NUMBER,
                        generator=GENERATOR,
                        client_public_key=CLIENT_PUBLIC_KEY,
                        server_private_key=SERVER_PRIVATE_KEY)
        SERVER.bind()
        SERVER.run()

    except Exception:
        sys.exit(2)
