# -*- coding: utf-8 -*-

"""
Aneta Ochedowska
"""

import random
import re
import hashlib
from Crypto.Cipher import AES
from Crypto.PublicKey import RSA


def variable_random(first):
    try:
        return random.randint(1, long(first) - 1)
    except Exception:
        raise Exception()


def mod_pow(gen, pow_val, p_first):
    try:
        return (long(gen) ** long(pow_val)) % long(p_first)
    except Exception:
        raise Exception()


def ruwnasie(string):
    try:
        length_message = len(string)
        add_ch = 512 - length_message
        return string + '=' * add_ch
    except Exception:
        raise Exception()


def del_ruwnasie(string):
    try:
        return re.sub('=*$', '', str(string))
    except Exception:
        raise Exception()


def sign(priv_key, message):
    with open(priv_key, 'r') as key_file:
        rsa = RSA.importKey(key_file.read())
        signed = rsa.sign(message, '')
        return str(signed[0])


def verify(pub_key, message, signature):
    with open(pub_key, 'r') as key_file:
        rsa = RSA.importKey(key_file.read())
        long_signature = long(signature)
        verified = rsa.verify(message, (long_signature, ''))
        return verified


def encrypt(message, key):
    try:
        sha_val = hashlib.sha256(str(key)).hexdigest()
        obj = AES.new(sha_val[:16], AES.MODE_CBC, sha_val[16:32])
        to_enc_message = ruwnasie(str(message))
        enc_message = obj.encrypt(to_enc_message)
        return enc_message
    except Exception:
        raise Exception()


def decrypt(message, key):
    try:
        sha_val = hashlib.sha256(str(key)).hexdigest()
        obj2 = AES.new(sha_val[:16], AES.MODE_CBC, sha_val[16:32])
        decoded = obj2.decrypt(str(message))
        decoded = re.sub('=*$', '', decoded)
        return decoded
    except Exception:
        raise Exception()


def sanitize(*msg):
    for m_s in msg:
        if re.match("ERROR", m_s):
            return True
